<?php

use Illuminate\Database\Seeder;
use App\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
         $this->call(EtudiantsTableSeeder::class);
         $this->call(AnneesTableSeeder::class);
         $this->call(MatieresTableSeeder::class);
         $this->call(ClassesTableSeeder::class);
         $this->call(ProfsTableSeeder::class);
         $this->call(CoursTableSeeder::class);
    }
}
