<?php

use Illuminate\Database\Seeder;

class AnneesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Model\annee::class,50)->create();
    }
}
