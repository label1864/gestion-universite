<?php

use Illuminate\Database\Seeder;

class ProfsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Model\Prof::class,50)->create();
    }
}
