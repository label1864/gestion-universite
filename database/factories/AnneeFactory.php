<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model\annee;
use App\Model\Etudiant;
use Illuminate\Support\Facades\DB;
use Faker\Generator as Faker;

$factory->define(annee::class, function (Faker $faker) {
    return [
        'année_academique' => rand(1990,2020),
        'etudiant_id' => Etudiant::all()->random(1)->first()->id,
    ];
});
