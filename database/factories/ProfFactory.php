<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model\Prof;
use Faker\Generator as Faker;

$factory->define(Prof::class, function (Faker $faker) {
    $lib = $faker->sentence($nbMots = 6, $variablenbMots = true);
    return [
        'nom' => $faker->name,
        'prénom' => $faker->lastName,
        'matricule' => rand(10000,20000),
        'nationalité' => $faker->country,
        'niveau' => Str::random(5),
        'grade' => Str::random(5),
    ];
});
