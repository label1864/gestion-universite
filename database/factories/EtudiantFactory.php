<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model\Etudiant;
use Faker\Generator as Faker;

$factory->define(Etudiant::class, function (Faker $faker) {
    $val = rand(0,100);
    $date = rand(1990,2020);
    return [
        'nom' => $faker->name,
        'prénom' => $faker->lastName,
        'nationalité' => $faker->country,
        'date_de_naissance' => date('Y-m-d'),
        'lieu_de_naissance' => $faker->country,
        'n_inscription' =>  '2019' . sprintf("%08d", $val),
        'année_academique' => rand(1990,2020),
        'Filière' => Str::random(5),
    ];
});
