<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model\Classe;
use App\Model\annee;
use Faker\Generator as Faker;

$factory->define(Classe::class, function (Faker $faker) {
    $lib = $faker->sentence($nbMots = 6, $variablenbMots = true);
    return [
        'libellé' => $lib,
        'année_id' => annee::all()->random(1)->first()->id,
    ];
});
