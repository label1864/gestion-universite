<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model\Matiere;
use Faker\Generator as Faker;

$factory->define(Matiere::class, function (Faker $faker) {
    $lib = $faker->sentence($nbMots = 6, $variablenbMots = true);
    return [
        'code_matière' =>rand(1000,2000),
        'libellé' => $lib,
    ];
});
