<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model\Cours;
use App\Model\Classe;
use App\Model\Prof;
use Faker\Generator as Faker;

$factory->define(Cours::class, function (Faker $faker) {
    $lib = $faker->sentence($nbMots = 6, $variablenbMots = true);
    return [
        'libellé' => $lib,
        'classe_libellé' => Classe::all()->random(1)->first()->libellé,
        'classe_id' => Classe::all()->random(1)->first()->id,
        'prof_id' => Prof::all()->random(1)->first()->id,
    ];
});
