<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Matiere extends Model
{
    use softDeletes;

    protected $fillable = ['code_matière', 'libellé'];


    public function annee()
    {
        return $this->belongTo(anee::class);
    }
}
