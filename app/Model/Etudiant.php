<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Etudiant extends Model
{
    use softDeletes;

    protected $table = 'etudiants';

   protected $fillable = ['nom', 'prénom','nationalité', 'date_de_naissance', 'lieu_de_naissance',
                            'n_inscription', 'filière', 'année_academique'];

    public function annee()
    {
        return $this->hasMany(annee::class);
    }
    public function matiereEtudiant()
    {
        return $this->hasOneThrough(Matiere::class, annee::class);
    }

}
