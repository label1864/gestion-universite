<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Classe extends Model
{
    use softDeletes;

    protected $fillable = ['libellé', 'année_id'];


    public function annee()
    {
        return $this->hasOne(annee::class, 'année_id');
    }
    public function cours()
    {
        return $this->hasMany(Cours::class, 'classe_id');
    }
}
