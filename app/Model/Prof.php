<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Prof extends Model
{
    use softDeletes;

    protected $fillable = ['nom', 'prénom', 'matricule', 'nationalité', 'niveau', 'grade'];

    public function cours()
    {
        return $this->hasMany(Cours::class, 'prof_id');
    }
}
