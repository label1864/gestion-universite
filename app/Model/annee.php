<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class annee extends Model
{
    use softDeletes;

    protected $fillable = ['année_academique', 'etudiant_id'];

    public function etudiant()
    {
        return $this->belongTo(Etudiant::class, 'etudiant_id');
    }
    public function classe()
    {
        return $this->hasMany(classe::class);
    }
    public function matiere()
    {
        return $this->hasMany(Matiere::class);
    }

}
