<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cours extends Model
{
    use softDeletes;

    protected $fillable = ['libellé', 'classe_id', 'prof_id', 'classe_libellé'];

    public function classe()
    {
        return $this->belongTo(Classe::class);
    }
    public function prof()
    {
        return $this->belongTo(Prof::class);
    }
}
