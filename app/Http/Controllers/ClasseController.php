<?php

namespace App\Http\Controllers;

use App\Model\Classe;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ClasseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $indexClasse = Classe::all();
        return $this->successfulMessage(200, 'Succesfull', true, $indexClasse->count(), $indexClasse);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $ClasseDataValidated = $request->validate([
            'libellé' => ['required', 'string', 'max:10'],
            'année_id' => ['exists:annees,id'],
        ]);

        if($ClasseDataValidated->fails())
        {
            return $this->response()->json(['error' => $ClasseDataValidated->error()], 422);
        }

        $Data = $request>all();
        $CreateClasse = Classe::create($Data);
        return $this->successfulMessage(200, 'Your Classe has been created', true, 1, $CreateClasse);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Classe  $classe
     * @return \Illuminate\Http\Response
     */
    public function show(Classe $classe)
    {
        $ShowClasse = Classe::findOrfail($classe);
        return $this->successfulMessage(200, 'Ok', true, 1, $ShowClasse);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Classe  $classe
     * @return \Illuminate\Http\Response
     */
    public function edit(Classe $classe)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Classe  $classe
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Classe $classe)
    {
        $UpdateClasse = DB::table('classes')
                           ->where('id', $classe)
                           ->update($request->all());
        return $this->successfulMessage(200, 'Your Classe had been updated', true,1, $UpdateClasse);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Classe  $classe
     * @return \Illuminate\Http\Response
     */
    public function destroy(Classe $classe)
    {
        $DestroyClasse = Classe::findOrfail($classe);

        if($DestroyClasse->delete())
        {
            return $this->successfulMessage(200, 'Your Classe has been well destroyed',true,1, $DestroyClasse);
        }
        else
        {
            return $this->errorMessage(427, ' Your attempt destroy has falied', false);
        }
    }
}
