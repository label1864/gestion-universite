<?php

namespace App\Http\Controllers;

use App\Model\Cours;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CoursController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $IndexCours = Cours::all();
        return $this->successfulMessage(200, 'Succesfull', true, $IndexCours->count(), $IndexCours);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $CoursDataValidated = $request->validate([
            'libellé' => ['required', 'string', 'max:15'],
            'classe_id' => ['exists:classes,id'],
        ]);

        if($CoursDataValidated->fails())
        {
            return $this->response()->json(['error' => $CoursDataValidated->errors()], 422);
        }

        $Data = $request>all();
        $CreateCours = Cours::create($Data);
        return $this->successfulMessage(200, 'Your Cours  has been created', true, 1, $CreateCours);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Cours  $cours
     * @return \Illuminate\Http\Response
     */
    public function show(Cours $cours)
    {
        $ShowCours = Cours::findOrfail($cours);
        return $this->successfulMessage(200, 'Your Cours  has been show', true, 1, $ShowCours);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Cours  $cours
     * @return \Illuminate\Http\Response
     */
    public function edit(Cours $cours)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Cours  $cours
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Cours $cours)
    {
        $UpdateCours = DB::table('cours')
                            ->where('id', $cours)
                            ->update($request>all());
        return $this->successfulMessage(200, 'Your Cours  has been update', true, 1, $UpdateCours);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Cours  $cours
     * @return \Illuminate\Http\Response
     */
    public function destroy(Cours $cours)
    {
        $DestroyCours = Cours::findOrfail($cours);
        
        if($DestroyCours->delete())
        {
            return $this->successfulMessage(200, 'Your Cours  has been destroy', true, 1, $DestroyCours);
        }
        else
        {
            return $this->errorMessage(427, ' Your attempt destroy has falied', false);
        }
    }
}
