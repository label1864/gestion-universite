<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Model\annee;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AnneeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $indexAnnee = annee::all();
        return $indexAnnee;
    } //successfulMessage(200, 'Ok', true, 1, $showAnnee);

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $AnneeDataValidated = $request->validate([
            'année_academique' => ['required','exists:App\Model\Etudiant, année_academique'],
            'etudiant_id' => ['exists:App\Model\Etudiant, id'],
        ]);

        if($AnneeDataValidated->fails())
        {
            return response()->json(['error' => $AnneeDataValidated->errors()], 422);
        }

        $Data = $request->all();
        $createAnnee = annee::create($Data);
        return $this->successfulMessage(200, 'Your Annee has been created', true, 1, $createAnnee);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\annee  $annee
     * @return \Illuminate\Http\Response
     */
    public function show(annee $annee)
    {
        //$showAnnee = annee::findOrFail($annee);
        return annee::findOrFail($annee);

        //$this->successfullMessage(200, 'Ok', true, 1, $showAnnee);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\annee  $annee
     * @return \Illuminate\Http\Response
     */
    public function edit(annee $annee)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\annee  $annee
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, annee $annee)
    {
        $updateAnnee = DB::table('annees')
                          ->where('id', $annee)
                          ->update($request->all());
        return $this->successfulMessage(200, 'Your Annee had been updated', true,1, $updateAnnee);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\annee  $annee
     * @return \Illuminate\Http\Response
     */
    public function destroy(annee $annee)
    {
        $DestroyAnnee = annee::findOrfail($annee);

        if($DestroyAnnee->delete())
        {
            return $this->successfulMessage(200, 'Your Annee has been well destroyed',true,1, $DestroyAnnee);
        }
        else
        {
            return $this->errorMessage(427, 'Your attempt failed', false);
        }
    }
}
