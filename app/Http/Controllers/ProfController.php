<?php

namespace App\Http\Controllers;

use App\Model\Prof;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProfController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $IndexProf = Prof::all();
        return $this->successfulMessage(200, 'Succesfull', true, $IndexProf->count(), $IndexProf);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {        
        $ProfDataValidated = $request->validate([

            'nom' => ['required', 'string'],
            'prénom' => ['required', 'string'],
            'matricule' => ['required'],
            'nationalité' => ['required', 'string'],
            'niveau' => ['required', 'string'],
            'grade' => ['required', 'string'],
        ]);

        if($ProfDataValidated->fails())
        {
            return $this->response()->json(['error' => $ProfDataValidated->errors()], 422);
        }

        $Data = $request->all();
        $CreateProf = Prof::create($Data);
        return $this->successfulMessage(200, 'Your Prof  has been created', true, 1, $CreateProf);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Prof  $prof
     * @return \Illuminate\Http\Response
     */
    public function show(Prof $prof)
    {
        $ShowProf = Prof::findOrfail($prof);
        return $this->successfulMessage(200, 'Your Prof  has been created', true, 1, $ShowProf);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Prof  $prof
     * @return \Illuminate\Http\Response
     */
    public function edit(Prof $prof)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Prof  $prof
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Prof $prof)
    {
        $UpdateProf = DB::table('profs')
                              ->where('id', $prof)
                              ->update($request->all());
        return $this->successfulMessage(200, 'Your Prof has been update', true, 1, $UpdateProf);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Prof  $prof
     * @return \Illuminate\Http\Response
     */
    public function destroy(Prof $prof)
    {
        $DestroyProf = Prof::findOrfail($prof);
        
        if($DestroyProf->delete())
        {
            return $this->successfulMessage(200, 'Your Prof  has been destroy', true, 1, $DestroyProf);
        }
        else
        {
            return $this->errorMessage(427, ' Your attempt destroy has falied', false);
        }
    }

}
