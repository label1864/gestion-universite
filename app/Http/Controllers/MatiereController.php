<?php

namespace App\Http\Controllers;

use App\Model\Matiere;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MatiereController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $IndexMatiere = Matiere::all();
        return $this->successfulMessage(200, 'Succesfull', true, $IndexMatiere->count(), $IndexMatiere);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $MatiereDataValidated = $request->validate([
            
            'code_matière' => ['required','max:10'],
            'libellé' => ['required', 'string', 'max:15'],
        ]);

        if($MatiereDataValidated->fails())
        {
            return $this->response()->json(['error' => $MatiereDataValidated->errors()], 422);
        }

        $Data = $request->all();
        $CreateMatiere = Matiere::create($Data);
        return $this->successfulMessage(200, 'Your Matiere  has been created', true, 1, $CreateMatiere);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Matiere  $matiere
     * @return \Illuminate\Http\Response
     */
    public function show(Matiere $matiere)
    {
        $ShowMatiere = Matiere::findOrfail($matiere);
        return $this->successfulMessage(200, 'Your Matiere  has been show', true, 1, $ShowMatiere);
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Matiere  $matiere
     * @return \Illuminate\Http\Response
     */
    public function edit(Matiere $matiere)
    {
        //
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Matiere  $matiere
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Matiere $matiere)
    {
        $UpdateMatiere = DB::table('matieres')
                              ->where('id', $matiere)
                              ->update($request->all());
        return $this->successfulMessage(200, 'Your Matiere has been update', true, 1, $UpdateMatiere);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Matiere  $matiere
     * @return \Illuminate\Http\Response
     */
    public function destroy(Matiere $matiere)
    {
        $DestroyMatiere = Matiere::findOrfail($matiere);
        
        if($DestroyMatiere->delete())
        {
            return $this->successfulMessage(200, 'Your Matiere  has been destroy', true, 1, $DestroyMatiere);
        }
        else
        {
            return $this->errorMessage(427, ' Your attempt destroy has falied', false);
        }
    }
}
