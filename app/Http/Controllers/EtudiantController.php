<?php

namespace App\Http\Controllers;

use App\Model\Etudiant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EtudiantController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $IndexEtudiant = Etudiant::all();
        return $this->successfulMessage(200, 'Succesfull', true, $IndexEtudiant->count(), $IndexEtudiant);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $EtudiantDataValidated = $request->validate([

            'nom' => ['required', 'string'],
            'prénom' => ['required', 'string'],
            'nationalité' => ['required', 'string'],
            'date_de_naissance' => ['required'],
            'lieu_de_naissance' => ['required', 'string'],
            'n_inscription' => ['required', 'string','max:20'],
            'filière' => ['required', 'string'],
            'année_academique' => ['required'],
        ]);

        if($EtudiantDataValidated->fails())
        {
            return $this->response()->json(['error' => $EtudiantDataValidated->errors()], 422);
        }

        $Data = $request->all();
        $CreateEtudiant = Etudiant::create($Data);
        return $this->successfulMessage(200, 'Your Etudiant  has been created', true, 1, $CreateEtudiant);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Etudiant  $etudiant
     * @return \Illuminate\Http\Response
     */
    public function show(Etudiant $etudiant)
    {
        $ShowEtudiant = Etudiant::findOrfail($cours);
        return $this->successfulMessage(200, 'Your Etudiant  has been show', true, 1, $ShowEtudiant);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Etudiant  $etudiant
     * @return \Illuminate\Http\Response
     */
    public function edit(Etudiant $etudiant)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Etudiant  $etudiant
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Etudiant $etudiant)
    {
        $UpdateEtudiants = DB::table('etudiants')
                            ->where('id', $etudiant)
                            ->update($request->all());
        return $this->successfulMessage(200, 'Your Etudiant  has been update', true, 1, $UpdateEtudiant);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Etudiant  $etudiant
     * @return \Illuminate\Http\Response
     */
    public function destroy(Etudiant $etudiant)
    {
        $DestroyEtudiant = Etudiant::findOrfail($etudiant);
        
        if($DestroyEtudiant->delete())
        {
            return $this->successfulMessage(200, 'Your Etudiant  has been destroy', true, 1, $DestroyEtudiant);
        }
        else
        {
            return $this->errorMessage(427, ' Your attempt destroy has falied', false);
        }
    }
}
