<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('/annees', 'AnneeController@index');
Route::post('/annees', 'AnneeController@store');
Route::get('/annees/{annee}', 'AnneeController@show');
Route::put('/annees/{id}', 'AnneeController@update');
Route::delete('/annees/{id}', 'AnneeController@destroy');

Route::get('/classes', '\ClasseController@index');
Route::post('/classes', '\ClasseController@store');
Route::get('/classes/{id}', '\ClasseController@show');
Route::put('/classes/{id}', '\ClasseController@update');
Route::delete('/classes/{id}', '\ClasseController@destroy');

Route::get('/cours', '\CoursController@index');
Route::post('/cours', '\CoursController@store');
Route::get('/cours/{id}', '\CoursController@show');
Route::put('/cours/{id}', '\CoursController@update');
Route::delete('/cours/{id}', '\CoursController@destroy');

Route::get('/etudiants', '\EtudiantController@index');
Route::post('/etudiants', '\EtudiantController@store');
Route::get('/etudiants/{id}', '\EtudiantController@show');
Route::put('/etudiants/{id}', '\EtudiantController@update');
Route::delete('/etudiants/{id}', '\EtudiantController@destroy');

Route::get('/matieres', '\MatiereController@index');
Route::post('/matieres', '\MatiereController@store');
Route::get('/matieres/{id}', '\MatiereController@show');
Route::put('/matieres/{id}', '\MatiereController@update');
Route::delete('/matieres/{id}', '\MatiereController@destroy');

Route::get('/profs', '\ProfController@index');
Route::post('/profs', '\ProfController@store');
Route::get('/profs/{id}', '\ProfController@show');
Route::put('/profs/{id}', '\ProfController@update');
Route::delete('/profs/{id}', '\ProfController@destroy');
